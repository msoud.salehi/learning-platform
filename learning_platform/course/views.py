from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from .models import Course
from .form import CreateCourseForm
from django.http import FileResponse
from django.contrib.auth.decorators import login_required


def pdf_view(request, pk):
    course_item = get_object_or_404(Course, pk=pk)
    response = FileResponse(course_item.pdf_file.open(), content_type='application/pdf')
    return response


def course_detail(request, pk):
    course_item = get_object_or_404(Course, pk=pk)
    context = {'course_item': course_item}
    return render(request, 'course/course_detail.html', context)


def create_course(request):
    if request.method == 'POST':
        form = CreateCourseForm(request.POST, request.FILES)
        if form.is_valid():
            var = form.save(commit=False)
            var.user = request.user
            var.save()
            messages.success(request, 'Course added')
            return redirect('all-courses')
        else:
            messages.warning(request, 'Something went wrong')
            return redirect('create-course')
    else:
        form = CreateCourseForm()
        context = {'form': form}
        return render(request, 'course/create_course.html', context)


# added 2 roles
@login_required
def all_courses(request):
    if request.user.role == 'teacher':
        obj = Course.objects.filter(user=request.user)
    elif request.user.role == 'student':
        # Adjust the query based on your logic for reviewers
        obj = Course.objects.all()
    else:
        # Handle other roles or unexpected scenarios
        obj = Course.objects.none()

    context = {'obj': obj}
    return render(request, 'course/all_courses.html', context)


