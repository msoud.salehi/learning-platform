from django.urls import path
from . import views, api_views


urlpatterns = [
    path('', views.all_courses, name='all-courses'),
    path('create_course/', views.create_course, name='create-course'),
    path('course/<int:pk>/', views.course_detail, name='course-detail'),
    path('pdfs/<int:pk>/', views.pdf_view, name='pdf_view'),
    path('api/courses/', api_views.CourseListAPIView.as_view(), name='api-course-list'),
]