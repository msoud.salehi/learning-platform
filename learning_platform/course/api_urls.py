# learning_platform/learning_app/learning_app/api_urls.py
from django.urls import path
from . import api_views

urlpatterns = [
    path('courses/', api_views.CourseListAPIView.as_view(), name='api-course-list'),
]
